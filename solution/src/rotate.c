#include "rotate.h"

struct image rotate(const struct image* const src) {
    struct image img = create_image(src->height, src->width);
    if (img.pixels == NULL) {
    	  return (struct image){0};
    }
    for (size_t i = 0; i < src->height; i++) {
        for (size_t j = 0; j < src->width; j++) {
            set_pixel(&img, get_pixel(src, i, j), j, src->height - i - 1);
        }
    }
    return img;
}
