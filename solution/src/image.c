#include "image.h"
#include <stdlib.h>

struct image create_image(uint64_t const width, uint64_t const height) {
	struct image img = {
	    .width = width,
	    .height = height,
	    .pixels = malloc(width * height * sizeof(struct pixel))
    };
	return img;
}

void destroy_image(struct image* const img) {
	free(img->pixels);
}

void set_pixel(struct image* img, struct pixel pixel, uint64_t const i, uint64_t const j) {
    img->pixels[img->width*i + j] = pixel;
}

struct pixel get_pixel(struct image const* img, uint64_t const i, uint64_t const j){
    return img->pixels[img->width*i + j];
}
