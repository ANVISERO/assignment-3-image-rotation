#include "bmp.h"
#include "file_manager.h"
#include "image.h"
#include "rotate.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    
    if (argc > 3) {
        fprintf(stderr, "There is too much arguments!");
        return 1;
    }
    
    if (argc < 3) {
        fprintf(stderr, "There is not enough arguments!");
        return 1;
    }

    FILE* in = NULL;
    FILE* out = NULL;

    
    if (file_open(&in, argv[1], "rb") != OPEN_OK) {
        fprintf(stderr, "Can not open input file to read!");
        return 1;
    }

    if (file_open(&out, argv[2], "wb") != OPEN_OK) {
        fprintf(stderr, "Can not open output file to write!");
        file_close(in);
        return 1;
    }


    struct image img = {0};
    
    if (from_bmp(in, &img)) {
        fprintf(stderr, "An error occurred while reading the input file!");
		file_close(in);
		file_close(out);
		return 1;
    }

    struct image rotated_img = rotate(&img);
    
    if (to_bmp(out, &rotated_img)) {
        fprintf(stderr, "An error occurred while writing the input file!");
        destroy_image(&rotated_img);
        destroy_image(&img);
		file_close(in);
		file_close(out);
		return 1;
    }

    

    destroy_image(&img);
    destroy_image(&rotated_img);

    if (file_close(in)) {
        fprintf(stderr, "An error occurred while closing the input file!");
        return 1;
    }

    if (file_close(out)) {
        fprintf(stderr, "An error occurred while closing the output file!");
        return 1;
    }

    

    return 0;
}
