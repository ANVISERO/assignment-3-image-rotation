#include "bmp.h"
#include <stdlib.h>

#define ZERO 0
#define NUMBER_OF_BITS 24
#define BF_TYPE 0x4D42
#define PLANES 1
#define SIZE 40


static enum read_status header_validate(struct bmp_header const* const header) {
    if (header->bfType != BF_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biBitCount != NUMBER_OF_BITS) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

static enum read_status get_header(FILE* const file, struct bmp_header* const header) {
	if (!fread(header, sizeof(struct bmp_header), 1, file)) {
		return READ_INVALID_BITS;
	}
	return header_validate(header);
}

static uint8_t get_padding(uint64_t const width) {
	return 4 - (uint8_t)((width * sizeof(struct pixel)) % 4);
}

static struct bmp_header create_bmp_header(const struct image* const  img) {
    uint32_t image_size = (sizeof(struct pixel) * img->width + get_padding(img->width)) * img->height;
	return (struct bmp_header) {
		.bfType = BF_TYPE,
	    .bfileSize = sizeof(struct bmp_header) + image_size,
		.bfReserved = ZERO,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = SIZE,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = PLANES,
		.biBitCount = NUMBER_OF_BITS,
		.biCompression = ZERO,
		.biSizeImage = image_size,
	    .biXPelsPerMeter = ZERO,
		.biYPelsPerMeter = ZERO,
	    .biClrUsed = ZERO,
		.biClrImportant = ZERO
	};
}

static enum write_status write_header(FILE* const file, const struct  bmp_header* const header) {
	if (fwrite(header, sizeof(struct bmp_header), 1, file) == 1) {
		return WRITE_OK;
	} 
	return WRITE_ERROR;
}

static enum read_status get_pixels(FILE* const file, struct image* const img) {
	struct image new_image = create_image(img->width, img->height);
	if (new_image.pixels == NULL) {
        	return READ_INVALID_BITS;
    }
	for (size_t i = 0; i < new_image.height; i++) {
		if (fread(new_image.pixels + i * new_image.width, sizeof(struct pixel), new_image.width, file) != new_image.width || 
			fseek(file, get_padding(new_image.width), SEEK_CUR) != 0) {
			free(new_image.pixels);
			return READ_INVALID_SIGNATURE;
		}
	}
	img->pixels = new_image.pixels;
	return READ_OK;
}

static enum write_status write_pixels(FILE* const file, const struct image* const img) {
	if (img == NULL || img->pixels == NULL) {
		return WRITE_ERROR;
	}
	for (size_t i = 0; i < img->height; ++i) {
		if (fwrite(img->pixels + i * img->width, sizeof(struct pixel), img->width, file) != img->width) {
			return WRITE_ERROR;
		}
		if (fseek(file, get_padding(img->width), SEEK_CUR) != 0) {
			return WRITE_ERROR;
		}
	}
	return WRITE_OK;
}

enum read_status from_bmp(FILE* const in, struct image* const img) {
	struct bmp_header header = {0};
	enum read_status status = get_header(in, &header);
	if (status == READ_OK) {
		img->width = header.biWidth;
		img->height = header.biHeight;
		return get_pixels(in, img);
	} 
	return status;
}


enum write_status to_bmp(FILE* const out, const struct image* const img) {
	struct bmp_header header = create_bmp_header(img);
	if (write_header(out, &header) == WRITE_OK) {
		return write_pixels(out, img);
	} 
	return WRITE_ERROR;
}
