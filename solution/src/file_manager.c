#include <file_manager.h>

enum open_status file_open(FILE** const file, const char* filename, const char* mode) {
    *file = fopen(filename, mode);
    if ((*file) == NULL) {
       return OPEN_ERROR;
    }
    return OPEN_OK;
}

enum close_status file_close(FILE* const file) {
    if (fclose(file) != 0) {
    	return CLOSE_ERROR;
    }
    return CLOSE_OK;
}
