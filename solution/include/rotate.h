#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"
#include <stdio.h>

struct image rotate(const struct image* const src);

#endif
