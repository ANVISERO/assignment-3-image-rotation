#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum open_status file_open(FILE** const file, const char* const filename, const char* const mode);

enum close_status file_close(FILE* const file);

#endif
