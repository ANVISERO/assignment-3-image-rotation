#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>


struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* pixels;
};


struct image create_image(uint64_t const width, uint64_t const height);

void destroy_image(struct image* const img);

void set_pixel(struct image* img, struct pixel pixel, uint64_t const i, uint64_t const j);

struct pixel get_pixel(struct image const* img, uint64_t const i, uint64_t const j);

#endif
